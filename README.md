<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# console 0.3.80

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_console/develop?logo=python)](
    https://gitlab.com/ae-group/ae_console)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_console/release0.3.79?logo=python)](
    https://gitlab.com/ae-group/ae_console/-/tree/release0.3.79)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_console)](
    https://pypi.org/project/ae-console/#history)

>ae_console module 0.3.80.

[![Coverage](https://ae-group.gitlab.io/ae_console/coverage.svg)](
    https://ae-group.gitlab.io/ae_console/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_console/mypy.svg)](
    https://ae-group.gitlab.io/ae_console/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_console/pylint.svg)](
    https://ae-group.gitlab.io/ae_console/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_console)](
    https://gitlab.com/ae-group/ae_console/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_console)](
    https://gitlab.com/ae-group/ae_console/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_console)](
    https://gitlab.com/ae-group/ae_console/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_console)](
    https://pypi.org/project/ae-console/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_console)](
    https://gitlab.com/ae-group/ae_console/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_console)](
    https://libraries.io/pypi/ae-console)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_console)](
    https://pypi.org/project/ae-console/#files)


## installation


execute the following command to install the
ae.console module
in the currently active virtual environment:
 
```shell script
pip install ae-console
```

if you want to contribute to this portion then first fork
[the ae_console repository at GitLab](
https://gitlab.com/ae-group/ae_console "ae.console code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_console):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_console/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.console.html
"ae_console documentation").
